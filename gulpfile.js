/*
 four top level functions

 gulp.task : defines tasks
 gulp.src : points to the file you want to use
 gulp.dest : points to output
 gulp.watch : two main forms. Both of which return an EventEmitter that emits change evetns.

 .pipe for chaining its output into other plugins

 */

const gulp         = require('gulp'),
      gutil        = require('gulp-util'),
      sass         = require('gulp-sass'),
      concat       = require('gulp-concat'),
      sourcemaps   = require('gulp-sourcemaps'),
      autoprefixer = require('gulp-autoprefixer'),
      restart      = require('gulp-restart'),
      uglify       = require('gulp-uglify'),
      htmlmin      = require('gulp-htmlmin'),
      refresh      = require('gulp-refresh'),
      imagemin     = require('gulp-imagemin')

// define the default task and add the watch task to it
gulp.task('default', ['watch'])

// min js
gulp.task('build-js', function () {
  return gulp.src('src/js/main.js')
    .pipe(sourcemaps.init())
    .pipe(concat('main.js'))
    //only uglify if gulp is ran with '--type production'
    .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/js'))
    .pipe(refresh())
})

// scss
gulp.task('build-css', function () {
  return gulp.src('src/scss/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('public/css'))
    .pipe(refresh())
})

// htmlmin
gulp.task('html-minify', function () {
  return gulp.src('src/*.html')
    .pipe(htmlmin({
      collapseWhitesoace: true,
      ignoreCustomFragments: [/<%[\s\S]*?%>/, /<\?[=|php]?[\s\S]*?\?>/]
    }))
    .pipe(gulp.dest('public'))
    .pipe(refresh())
})

// php copy classes
gulp.task('copy1', function () {
  gulp.src('src/php/classes/**/*.php')
    .pipe(gulp.dest('./public/php/classes'));
});

// php copy config
gulp.task('copy2', function () {
  gulp.src('src/php/config/**/*.php')
    .pipe(gulp.dest('./public/php/config'));
});

//copy php files
gulp.task('copy3', function () {
  gulp.src('src/**/*.php')
    .pipe(gulp.dest('./public'));
});

// copy lib files
gulp.task('copyLibs', function () {
  gulp.src('src/js/libs/**/*.js')
    .pipe(gulp.dest('./public/js/libs'))
})

gulp.task('img-min', () =>
gulp.src('src/img/*')
  .pipe(gulp.dest('public/img'))
);

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function () {
  refresh.listen({basePath: 'public', cache: false})
  refresh.listen({basePath: 'src', cache: false})
  gulp.watch('src/js/**/*.js', ['build-js'])
  gulp.watch('src/scss/**/*.scss', ['build-css'])
  gulp.watch('src/**/*.html', ['html-minify'])
  gulp.watch('src/php/classes/**/*.php', ['copy1']);
  gulp.watch('src/php/config/**/*.php', ['copy2']);
  gulp.watch('src/**/*.php', ['copy3']);
  gulp.watch('src/js/libs/**/*.php', ['copyLibs'])
  gulp.watch('src/img/*', ['img-min'])
})